
export class User {
    /** l'attribut public dans le constructeur créer automatiquement les attributs
    firstName: string;
    lastName: string;
     ? pour optionnel
    **/
    constructor(
                public type: string,
                public avatar:string,
                public nom: string,
                public prenom: string,
                public email: string,
                public password:string,
                public phone: string,
                public entrepriseCheck:boolean,
                public isValid:boolean,   
                public inputEntreprise?: string,
                public inputEntrepriseTVA?: string,
                public inputEntreprisePresta?: number,
                public inputEntrepriseAdressePresta?: string,
                public inputEntrepriseTVAPresta?: string,
                public inputEntrepriseIBANPresta?: string,    
                public id?
              ) {
  
    }
}
  