import { Component, OnInit, ChangeDetectorRef} from '@angular/core';
import { AbstractControl, NgForm, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { SearchCountryField, TooltipLabel, CountryISO } from 'ngx-intl-tel-input';
import { User } from '../modeles/user';
import { invalid } from '@angular/compiler/src/render3/view/util';
import { MatDialog } from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material/dialog';
import { UpdateProfilInfoComponent } from '../update-profil-info/update-profil-info.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LOADIPHLPAPI } from 'dns';
import { ValidationInfoModalComponent } from '../validation-info-modal/validation-info-modal.component';
import { AuthenticationService } from '../services/authentication.service';
import { Subscription } from 'rxjs';
import {  FileUploader, FileSelectDirective } from 'ng2-file-upload';
import { ChangePasswordComponent } from '../change-password/change-password.component';






@Component({
  selector: 'app-edit-profil',
  templateUrl: './edit-profil.component.html',
  styleUrls: ['./edit-profil.component.scss']
})

export class EditProfilComponent implements OnInit {
  userChange:User;
  currentUser: User;
  userForm: FormGroup;
  phone:any;
  currentUserSubscription: Subscription;
  SERVER_URL = "https://test.keepupcar.ch/assets/userPicture";
  uploadForm: FormGroup;
  
  



  constructor(public dialogRef: MatDialogRef<EditProfilComponent>, private formBuilder:FormBuilder, public dialog: MatDialog, 
    private httpClient: HttpClient, private authService: AuthenticationService, private cd: ChangeDetectorRef) { }


    preUserType = '';
    SearchCountryField = SearchCountryField;
    TooltipLabel = TooltipLabel;
    CountryISO = CountryISO;
    preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
   
    

    ngOnInit() {
      // this.currentUser = this.authService.getCurrentUserValue();
      this.currentUserSubscription = this.authService.currentUserSubject.subscribe(
        currentUserFromSubject => {
          this.currentUser = currentUserFromSubject; 
          this.initForm();
          console.log(this.userForm.get('phone'));
        }
      );
      this.authService.emitUserAndIsAuth();
      this.uploadForm = this.formBuilder.group({
        profile: [''],
      });
    }
    /*getUser(){
      return JSON.parse(localStorage.getItem('user'));
    }*/
    initForm(){
      this.userForm = this.formBuilder.group({
        type: ['Client'],
        avatar:'',
        nom: [this.currentUser['nom'], Validators.required],
        prenom: [this.currentUser['prenom'], Validators.required],
        email: [this.currentUser['email'], [Validators.required, Validators.email]],
        //password: ['', [Validators.required, Validators.maxLength(20), Validators.minLength(6)]],
        //confirmPassword: ['', [Validators.required, Validators.maxLength(20), Validators.minLength(6)]],
        phone: ['', Validators.required],
        entrepriseCheck: this.currentUser['entrepriseCheck'],
        isValid: this.currentUser['isValid'],
        inputEntreprise:'',
        inputEntrepriseTVA:'',
      });
    }
    onFileSelect(event) {
      if (event.target.files.length > 0) {
        const file = event.target.files[0];
        this.uploadForm.get('profile').setValue(file, {emitModelToViewChange: false});
      }
      
    }
    onSubmit() {
      const formData = new FormData();
      formData.append('file', this.uploadForm.get('profile').value);
      this.httpClient.post<any>(this.SERVER_URL, formData).subscribe(
        (res) => console.log(res),
        (err) => console.log(err)
      );
    }
    changePreferredCountries() {
      this.preferredCountries = [CountryISO.Switzerland, CountryISO.France];
    }
    passwordConfirming(c: AbstractControl): { invalid: boolean } {
      if (c.get('password').value !== c.get('confirmPassword').value) {
          return {invalid: true};
      }
    }
    submitForm(){
      const formValue = this.userForm.value;
      this.userChange = new User(
        formValue['type'],
        formValue['avatar'],
        formValue['nom'],
        formValue['prenom'],
        formValue['email'],
        //formValue['password'],
        formValue['phone']["e164Number"],
        formValue['entrepriseCheck'],
        formValue['isValid'],
        formValue['inputEntreprise'],
        formValue['inputEntrepriseTVA'],
        formValue['inputEntreprisePresta'],
        formValue['inputEntrepriseAdressePresta'],
        formValue['inputEntrepriseTVAPresta'],
        formValue['inputEntrepriseIBANPresta'],    
      );
      this.editUser(this.userChange);
      console.log(this.userChange);
      this.dialogRef.close();
    }

    openDialog(): void {
      
      const dialogRef = this.dialog.open(UpdateProfilInfoComponent, {
        width: '300px',
        height: '200px',
      })
    }
    changePassword(): void {
        const dialogRef = this.dialog.open(ChangePasswordComponent, {
        width: '600px',
        height: '800px',
      })
    }
    onEntrepriseCheckChange(){
      if (this.userForm.value['entrepriseCheck']) {
        this.userForm.addControl('inputEntreprise', new FormControl('', Validators.required));
        this.userForm.addControl('inputEntrepriseTVA', new FormControl('', Validators.required));
      } else{
        this.userForm.removeControl('inputEntreprise');
        this.userForm.removeControl('inputEntrepriseTVA');
      }
    }
    
    editUser(user: User) {
      // Créer l'utilisateur dans la base de donnée
      // récupére l'id de l'utilisateur sur le serveur
      // si cors error pensez à vider le cache
      const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
      this.httpClient.post('https://test.keepupcar.ch/backend_keepupcar/updateUser.php', user ,  { headers: headers}).subscribe((result) => {
          console.log('success'); // TODO: AFFICHER UN MESSAGE QUE EN CAS DE REUSSITE
          this.openDialog();
          console.log(result);
        }, (error) => {
           console.log('error');
          console.log(error);
          alert("Le mail renseigné est déjà raccordé à un compte");
          // this.inscriptionMessageSubject.next('Server post - Erreur ! ' + error);
        } );
      }
      
}
