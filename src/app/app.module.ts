import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSliderModule } from '@angular/material/slider';
import {MatDialogModule} from '@angular/material/dialog';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule, MAT_RADIO_DEFAULT_OPTIONS} from '@angular/material/radio';
import {MatTabsModule} from '@angular/material/tabs';
import { FileSelectDirective } from 'ng2-file-upload';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoardComponent } from './board/board.component';
import { UserProfilComponent } from './user-profil/user-profil.component';
import { UserService } from './services/user.service';
import { CarService } from './services/car.service';
import { fakeCarBddService } from './services/fakeCarBdd.service';
import { fakeCommandesPrestaService } from './services/fakeCommandePrestaBdd.service';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { NgbdModalComponent, NgbdModalContent } from './modal/modal.component';
import { RegistreComponent } from './registre/registre.component';
import { PrestataireComponent } from './prestataire/prestataire.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrestataireService } from './services/prestataire.service';
import { BoardPrestaComponent } from './board-presta/board-presta.component';
import { TarificationFormPrestaComponent } from './tarification-form-presta/tarification-form-presta.component';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuClientComponent } from './menu-side/menu-side.component';
import { PrestaListeComponent } from './presta-liste/presta-liste.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CalendarComponent } from './calendar/calendar.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeFrExtra from '@angular/common/locales/extra/fr';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { CarFormComponent } from './car-form/car-form.component';
import { PrestaFormComponent } from './presta-form/presta-form.component';
import { CommandesPrestaComponent } from './commandes-presta/commandes-presta.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import { DetailCommandeComponent } from './detail-commande/detail-commande.component';
import {MatButtonModule} from '@angular/material/button';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import {BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgxIntlTelInputModule} from 'ngx-intl-tel-input';
import { ValidationInfoModalComponent } from './validation-info-modal/validation-info-modal.component';
import { HttpClientModule } from '@angular/common/http';
import { ValidateUserComponent } from './validate-user/validate-user.component';
import { AlertComponent } from './alert/alert.component';
import { AuthGuard } from './services/authGuard.service';
import { EditProfilComponent } from './edit-profil/edit-profil.component';
import { UpdateProfilInfoComponent } from './update-profil-info/update-profil-info.component';
import { UpdateProfilPictureComponent } from './update-profil-picture/update-profil-picture.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import {MatStepperModule} from '@angular/material/stepper';


registerLocaleData(localeFr, 'FR', localeFrExtra);
const AppRoutes: Routes = [
  { path:'', component: AuthComponent },
  { path:'profil',canActivate: [AuthGuard], component: UserProfilComponent},
  { path:'voitures',canActivate: [AuthGuard], component: BoardComponent},
  { path:'registre', component: RegistreComponent},
  { path:'form', component: NgbdModalComponent},
  { path:'boardPresta',canActivate: [AuthGuard], component: BoardPrestaComponent},
  { path: 'tarifFormPresta', canActivate: [AuthGuard],component: TarificationFormPrestaComponent },
  { path: 'listePresta', canActivate: [AuthGuard],component: PrestaListeComponent },
  { path: 'calendar', canActivate: [AuthGuard],component: CalendarComponent},
  { path: 'carForm', canActivate: [AuthGuard],component: CarFormComponent},
  { path: 'prestaForm', canActivate: [AuthGuard],component: PrestaFormComponent},
  { path: 'commandesPresta', canActivate: [AuthGuard],component: CommandesPrestaComponent},
  { path :'validateUser' , component: ValidateUserComponent}

]
@NgModule({
  declarations: [
    AppComponent,
    BoardComponent,
    UserProfilComponent,
    AuthComponent,
    NgbdModalComponent,
    NgbdModalContent,
    RegistreComponent,
    PrestataireComponent,
    BoardPrestaComponent,
    TarificationFormPrestaComponent,
    MenuClientComponent,
    PrestaListeComponent,
    CalendarComponent,
    CarFormComponent,
    PrestaFormComponent,
    CommandesPrestaComponent,
    DetailCommandeComponent,
    ValidationInfoModalComponent,
    ValidateUserComponent,
    AlertComponent,
    EditProfilComponent,
    UpdateProfilInfoComponent,
    FileSelectDirective,
    UpdateProfilPictureComponent,
    ChangePasswordComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule.forRoot(AppRoutes), 
    NgbModule,
    FormsModule,
    MatInputModule,
    BrowserAnimationsModule,
    NgbModalModule,
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }),
    FilterPipeModule,
    MatSliderModule,
    MatDialogModule,
    MatCheckboxModule,
    MatRadioModule,
    MatFormFieldModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatButtonModule,
    MatExpansionModule,
    MatCardModule,
    BsDropdownModule.forRoot(),
    NgxIntlTelInputModule,
    MatStepperModule
  ],
  exports: [
    DetailCommandeComponent
  ],
  entryComponents: [
    DetailCommandeComponent,
    ValidationInfoModalComponent,
    EditProfilComponent,
    UpdateProfilInfoComponent,
    UpdateProfilPictureComponent,
    ChangePasswordComponent
  ],
  providers: [
    UserService,
    CarService,
    PrestataireService,
    fakeCarBddService,
    fakeCommandesPrestaService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
//import {MatInputModule} from '@angular/material/input';