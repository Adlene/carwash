export class fakeCarBddService{
    fakeCarBdd=[
        {
            marque: 'Renault',
            model: 'Clio',
        },
        {
            marque: 'Renault',
            model: 'Twingo',
        },
        {
            marque: 'Tesla',
            model: 'S100D',
        },
        {
            marque: 'Opel',
            model: 'Astra',
        },
        {
            marque: 'Opel',
            model: 'Vectra',
        },
        {
            marque: 'Opel',
            model: 'Corsa',
        },
        {
            marque: 'Peugeot',
            model:'206',
        },
        {
            marque: 'Peugeot',
            model:'306',
        },
        {
            marque: 'Peugeot',
            model:'406',
        },
        {
            marque: 'Peugeot',
            model:'806',
        },
        {
            marque: 'Ford',
            model: 'Mondeo',
        },
        {
            marque: 'Ford',
            model: 'Focus',
        },
        {
            marque: 'Ford',
            model: 'Fiesta',
        },
    ]
    categories = ['Ford','Opel','Peugeot','Renault','Tesla'];
    
    
    
}