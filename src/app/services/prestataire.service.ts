export class PrestataireService{
    prestataire = [
        {
            label : "CarMania",
            password: " ",
            serviceDescription: "Entretien, révision sur tous types de véhicules",
        },
        {
            label : "NetCar",
            password: " ",
            serviceDescription: "Nettoyage auto de qualité, intérieur et extérieur, tous types de véhicules",
        },
        {
            label : "MechanoCar",
            password: "",
            serviceDescription: "Entretien, révision sur tous types de véhicules",
        },
        {
            label : "CarWashing",
            password: "",
            serviceDescription: "Nettoyage auto de qualité, intérieur et extérieur, tous types de véhicules",
        },
        {
            label : "CarStyle",
            password: "",
            serviceDescription: "Nettoyage auto de qualité, intérieur et extérieur, tous types de véhicules",
        },
        {
            label : "AssurAuto",
            password: "",
            serviceDescription: "Entretien, révision sur tous types de véhicules"
        },
        {
            label : "AutoPassion",
            password: "",
            serviceDescription: "Nettoyage auto de qualité, intérieur et extérieur, tous types de véhicules"
        },
    ]
    
}