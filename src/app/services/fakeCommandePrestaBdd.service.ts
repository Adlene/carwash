export class fakeCommandesPrestaService{

    fakeCommandesPresta = [
        {
            id : '001',
            statut : 'Demandé',
            client : 'Menot',
            date : new Date,
            lieu : 'Société Google',
            commentaire: 'Récupérer les clés à la réception',
            horaire: 'matin',
            véhicule: 'Aston Martin db11',
            type: 'intérieur'        
        },
        {
            id : '002',
            statut : 'Payé',
            client : 'Murciano',
            date : new Date,
            lieu : 'Parking rue de Berne',
            commentaire: '', 
            horaire: 'matin',
            véhicule: 'Audi Rs4',
            type: 'intérieur'  
        },
        {
            id : '003',
            statut : 'Effectué',
            client : 'Musk',
            date : new Date,
            lieu : 'Antenne Tesla Geneve',
            commentaire : 'Mot de passe ouverture: armagedon',  
            horaire: 'matin',
            véhicule: 'Lamborghini Aventador',
            type: 'extérieur'  
        },
        {
            id : '004',
            statut : 'Effectué',
            client : 'Musk',
            date : new Date,
            lieu : 'Antenne Tesla Geneve',
            commentaire : 'Mot de passe ouverture: armagedon',
            horaire: 'apres-midi',
            véhicule: 'Aston Martin db11',
            type: 'intérieur'  
        },
        {
            id : '005',
            statut : 'Effectué',
            client : 'Dupond',
            date : new Date,
            lieu : 'ONU Geneve',
            commentaire : 'Clés au poste de gardiennage entrée nord ',
            horaire: 'matin',
            véhicule: 'Bugatti Chiron',
            type: 'intérieur'  
        },
        {
            id : '006',
            statut : 'Demandé',
            client : 'Winston',
            date : new Date,
            lieu : 'Palais Winston',
            commentaire : '',
            horaire: 'matin',
            véhicule: 'Nissan Quashquai',
            type: 'extérieur'  
        },
        {
            id : '007',
            statut : 'Effectué',
            client : 'Spring',
            date : new Date,
            lieu : 'Conforama Meyrin',
            commentaire : '',
            horaire: 'matin',
            véhicule: 'Bentley continental GT',
            type: 'intérieur'  
        }

    ]
}