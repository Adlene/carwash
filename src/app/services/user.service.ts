import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';


@Injectable()

export class UserService{
    
    isAuth:boolean ; 
    currentUser ;
    authSubject = new Subject<boolean>();
    userConnectedSubject = new Subject<any>(); // enregistre les informations utilisateur
    //authSubject = new Subject<boolean>();
    //userConnectedSubject = new Subject<any>();

    user_prestataire = 
    {
        lastName: 'carWasher',
        firstName: 'johnny',
        password: 'xxteslaxx',
        mail: 'johnny.gmail.com',
        avatar: './assets/userPicture/elonMusk.jpg',
        age: 52,
        vehiculePossessed: 4,
        dateCreat: new Date,
        status: 'prestataire'
    }
    user_client = 
    {
        lastName: 'Musk',
        firstName: 'Elon',
        password: 'xxteslaxx',
        mail: 'elonMusk.gmail.com',
        avatar: './assets/userPicture/elonMusk.jpg',
        age: 48,
        vehiculePossessed: 3,
        dateCreat: new Date,
        status: 'client'
    }
    constructor(private router: Router){
        //this.currentUser = this.user_prestataire
    }
   
    login(type) {
        console.log('loggin en cours');
        console.log(type);
        this.isAuth = true; 
        this.authSubject.next(true); // connecté dynamique pour header

        if(type==='client'){
            this.currentUser = this.user_client;
            this.router.navigateByUrl('voitures');
        }else if(type==='prestataire'){
            this.currentUser = this.user_prestataire;
            this.router.navigateByUrl('tarifFormPresta');
        }
       
        this.userConnectedSubject.next(this.currentUser); // dynamique pour header emet les informations utilisateur
        console.log('currentUserChange');
        console.log(this.currentUser);
        
        //this.authSubject.next(true); // connecté dynamique pour header
       // this.userConnectedSubject.next(this.currentUser); // dynamique pour header emet les informations utilisateur
        
    }  
    getUserConnected() {
        console.log('current User');
        console.log(this.currentUser);
        return this.currentUser;
    }
    getIsAuth(){
        return this.isAuth;
    }
    logout() {
        this.isAuth = false; 
        this.authSubject.next(false); // connecté dynamique pour header
        this.router.navigateByUrl('');
    }     
   
}