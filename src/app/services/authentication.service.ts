import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../modeles/user';
import { ValidationInfoModalComponent } from '../validation-info-modal/validation-info-modal.component';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    public currentUserSubject: BehaviorSubject<User>;
    public currentUser: User;
    isAuth: boolean = false;
    isAuthSubject= new Subject<boolean>();

    constructor(private httpClient: HttpClient, private router: Router) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
        if (this.currentUserSubject.value) {
            this.currentUser = this.currentUserSubject.value;
            this.isAuth = true;
            this.emitUserAndIsAuth();
        }
    }
/*
    public getCurrentUserValue(): User {
        return this.currentUser;
    }*/
    emitUserAndIsAuth() {
        console.log('emit user');
        console.log(this.currentUser);
        this.currentUserSubject.next(this.currentUser);
        this.isAuthSubject.next(this.isAuth);
    }
   /* login(username, password) {
        return this.httpClient.post<any>('http://test.keepupcar.ch/backend_keepupcar/authentication.php', { username, password })
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return user;
            }));
    }*/
    createNewUser(user: User, callback) {
        // Créer l'utilisateur dans la base de donnée
        // récupére l'id de l'utilisateur sur le serveur
        // si cors error pensez à vider le cache
        const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
        this.httpClient.post('https://test.keepupcar.ch/backend_keepupcar/createNewUser.php', user ,  { headers: headers}).subscribe((result) => {
            console.log('success'); // TODO: AFFICHER UN MESSAGE QUE EN CAS DE REUSSITE
            callback();
            console.log(result);
          }, (error) => {
            console.log('error');
            console.log(error);
            alert("Le mail renseigné est déjà raccordé à un compte");
            // this.inscriptionMessageSubject.next('Server post - Erreur ! ' + error);
          } );
    }
    signIn(authForm) {
        const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
        this.httpClient.post('https://test.keepupcar.ch/backend_keepupcar/authenticate.php', authForm ,  { headers: headers}).subscribe(
        (result) => {
            console.log(result);
            if(result['isAuth']) {//si isAuth est true on transfert l'objet a la variable currentUser
                console.log('success_server_return'); // TODO: AFFICHER UN MESSAGE QUE EN CAS DE REUSSITE
                console.log(result);
                // this.currentUser = result; // A REMPLACER 
                       
                let tempUser: User = {
                    type: (result['AccountType']==="1")? "client":"prestataire",
                    avatar : result['avatar'],
                    nom :result['LastName'],
                    prenom: result['FirstName'],
                    email: result['email'],
                    password: result['password'],
                    phone : result['phone'],
                    entrepriseCheck:  (result['IsEntreprise']==="1"),
                    isValid : (result['IsValid']==="1"),
                    id: result['UserId']
                };
               // tempUser.inputEntreprise = result['Entreprise']
            
                this.currentUser = tempUser; // TODO : modifier 
                this.isAuth = true;
                localStorage.setItem('user', JSON.stringify(this.currentUser));
                this.emitUserAndIsAuth();
                if(this.currentUser.type == 'client' && this.currentUser.isValid ) {
                    this.router.navigate(['/voitures'])
                }
                else if(this.currentUser.type == 'prestataire' && this.currentUser.isValid) {
                    this.router.navigate(['/commandesPresta'])
                }
                else {
                    alert('Vous devez activer votre compte pour vous connecter')
                }
            } else {
                alert('Mail ou mot de passe incorrect')
            }
            
        }, (error) => {
            console.log('error');
            console.log(error);
            // this.inscriptionMessageSubject.next('Server post - Erreur ! ' + error);
        } 
        );
    }

    signOut() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('user');
        this.currentUser= null;
        this.isAuth = false;
        this.emitUserAndIsAuth();
    }
}