export class CarService{

    cars = [
        {
            name: 'Maserati Lavante ',
            picture: '../assets/carsPictures/maseratiLevante.jpg',
            revision: 60,
            interClean: 75,
            extClean: 75,
            marque: 'Maserati',
            model: 'Lavante',
            type: 'SUV',
            immat: 'xx-123-xx',
            couleur: 'noir metallisé',
            dateMC: '03/10/19',
            motorisation: 'v8',
            pneuEte: '21p',
            pneuHiver: '21p',
            leasing: 'Maserati',
            contractNo: 'A1234',
            assurance: 'Axa'

        },
        {
            name: 'Tesla S100D',
            picture: '../assets/carsPictures/teslaModelS100D.jpg',
            revision: 40,
            interClean: 35,
            extClean: 15,
            marque: 'Tesla',
            model: 'LS100D',
            type: 'Electrique',
            immat: 'xx-123-xx',
            couleur: 'Rouge électrique',
            dateMC: '03/10/19',
            motorisation: 'Electrique',
            pneuEte: '21p',
            pneuHiver: '21p',
            leasing: 'Tesla',
            contractNo: 'A1234',
            assurance: 'Axa'
        },
        {
            name: 'Tesla XP100D SportLine',
            picture: '../assets/carsPictures/teslaModelXP100DSportLine.jpg',
            revision: 100,
            interClean: 100,
            extClean: 100,
            marque: 'Tesla',
            model: 'XP100D SportLine',
            type: 'Electrique',
            immat: 'xx-123-xx',
            couleur: 'Noir',
            dateMC: '02/10/19',
            motorisation: 'Electrique',
            pneuEte: '21p',
            pneuHiver: '21p',
            leasing: 'Tesla',
            contractNo: 'A1234',
            assurance: 'Axa'
        }
    ]
}