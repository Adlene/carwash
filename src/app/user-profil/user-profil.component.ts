import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { CarService } from '../services/car.service';
import { MatDialog } from '@angular/material/dialog';
import { EditProfilComponent } from '../edit-profil/edit-profil.component';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';
import { User } from '../modeles/user';
import { UpdateProfilPictureComponent } from '../update-profil-picture/update-profil-picture.component';

@Component({
  selector: 'app-user-profil',
  templateUrl: './user-profil.component.html',
  styleUrls: ['./user-profil.component.scss']
})
export class UserProfilComponent implements OnInit {
  
  currentUser: User;
  cars:any[];
  currentUserSubscription: Subscription;
  constructor(private userService: UserService, private carService: CarService, public dialog: MatDialog, private authService: AuthenticationService) { }
  openDialog(): void {
    const dialogRef = this.dialog.open( UpdateProfilPictureComponent, {
      width: '70%',
      height: '100%',
    })
  }
  openDialog1(): void {
    const dialogRef = this.dialog.open( EditProfilComponent, {
      width: '70%',
      height: '100%',
    })
  }
  

  ngOnInit() {
    this.cars = this.carService.cars;
    this.currentUserSubscription = this.authService.currentUserSubject.subscribe(
      currentUserFromSubject => {
        this.currentUser = currentUserFromSubject; 
        }
    );
    this.authService.emitUserAndIsAuth();


  }

}
