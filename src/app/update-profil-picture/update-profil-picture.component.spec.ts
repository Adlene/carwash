import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateProfilPictureComponent } from './update-profil-picture.component';

describe('UpdateProfilPictureComponent', () => {
  let component: UpdateProfilPictureComponent;
  let fixture: ComponentFixture<UpdateProfilPictureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateProfilPictureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateProfilPictureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
