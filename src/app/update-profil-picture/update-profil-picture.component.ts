import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-update-profil-picture',
  templateUrl: './update-profil-picture.component.html',
  styleUrls: ['./update-profil-picture.component.scss']
})
export class UpdateProfilPictureComponent implements OnInit {
  SERVER_URL = "https://test.keepupcar.ch/assets/userPicture";
  uploadForm: FormGroup;  
  constructor(private formBuilder: FormBuilder, private httpClient: HttpClient) { }

  ngOnInit() {
    this.uploadForm = this.formBuilder.group({
      profile: ['']
    });
  }
  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get('profile').setValue(file);
      console.log('fichier changé');
    }
  }
  onSubmit() {
    const formData = new FormData();
    formData.append('file', this.uploadForm.get('profile').value);

    this.httpClient.post<any>(this.SERVER_URL, formData).subscribe(
      (res) => console.log(res, 'resultat cool'),
      (err) => console.log(err, 'resultat pas cool')
    );
  }
}
