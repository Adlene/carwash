import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../services/user.service';
import { PrestataireService } from '../services/prestataire.service';
import { logging } from 'protractor';
import {MatDialog} from '@angular/material/dialog';
import { RegistreComponent } from '../registre/registre.component';
import {AbstractControl, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Router} from "@angular/router"
import { Subject, Subscription } from 'rxjs';
import { User } from '../modeles/user';
import { AuthenticationService } from '../services/authentication.service';

@Component({
selector: 'app-auth',
templateUrl: './auth.component.html',
styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

prestataire: any[];
user;
userType = '';
authForm: FormGroup;
currentUser: User;
currentUserSubscription: Subscription;

isAuth: boolean;
isAuthSubscription: Subscription;

constructor( private router: Router, private httpClient: HttpClient, private userService: UserService, 
  private prestataireService: PrestataireService, public dialog: MatDialog, private formBuilder:FormBuilder,
  private authService: AuthenticationService) { }

openDialog(): void {
  const dialogRef = this.dialog.open(RegistreComponent, {
    width: '70%',
    height: '100%',
  })

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
  });
}

ngOnInit() { // s'execute une fois à l'initialisation du composant 
  this.initForm();
  this.currentUserSubscription = this.authService.currentUserSubject.subscribe(
    currentUserFromSubject => {
      this.currentUser = currentUserFromSubject; 
      }
  );
  this.isAuthSubscription = this.authService.isAuthSubject.subscribe(
    isAuthFromSubject => {this.isAuth = isAuthFromSubject;}
  );
  this.authService.emitUserAndIsAuth();

  if(this.isAuth && this.currentUser.type == 'client'){
    this.router.navigate(['/voitures'])
  }
  else if(this.isAuth && this.currentUser.type == 'prestataire'){
      this.router.navigate(['/commandesPresta'])
  }
}
initForm(){
  this.authForm = this.formBuilder.group({
    mail: ['', [Validators.required, Validators.email]],
    password: ['',  [Validators.required, Validators.maxLength(20), Validators.minLength(6)]]
})
}

onSubmit(){
  const authValue = this.authForm.value;
  console.log(authValue);
  this.authService.signIn(authValue);
}
}

