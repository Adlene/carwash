import { Component, OnInit } from '@angular/core';
import { PrestataireService } from '../services/prestataire.service';
import { CarService } from '../services/car.service';

  

@Component({
  selector: 'app-presta-liste',
  templateUrl: './presta-liste.component.html',
  styleUrls: ['./presta-liste.component.scss']
})

export class PrestaListeComponent implements OnInit {

  prestataires: any[] ;
  cars = [];
  prestaFilter: any = { label: '' };
  // prestaFilter1: any ={ $or :''};

  constructor( private prestataireService: PrestataireService, private carService:CarService) { }

  ngOnInit() {
    
    this.prestataires = this.prestataireService.prestataire;
    this.cars = this.carService.cars;
    // this.prestaFilter1 = this.prestataireService.prestataire[2] ;
  }

}
