import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrestaListeComponent } from './presta-liste.component';

describe('PrestaListeComponent', () => {
  let component: PrestaListeComponent;
  let fixture: ComponentFixture<PrestaListeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrestaListeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrestaListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
