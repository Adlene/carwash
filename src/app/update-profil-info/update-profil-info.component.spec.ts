import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateProfilInfoComponent } from './update-profil-info.component';

describe('UpdateProfilInfoComponent', () => {
  let component: UpdateProfilInfoComponent;
  let fixture: ComponentFixture<UpdateProfilInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateProfilInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateProfilInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
