import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatStepperModule} from '@angular/material/stepper';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  constructor(private _formBuilder: FormBuilder, private httpClient: HttpClient) {}

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      password: ['',  [Validators.required, Validators.maxLength(20), Validators.minLength(6)]]
    });
    this.secondFormGroup = this._formBuilder.group({
      newPassword: ['', [Validators.required, Validators.maxLength(20), Validators.minLength(6)]]
    });
  }
  checkPassword(){
    let Form = JSON.stringify(this.firstFormGroup.value);
    console.log(Form)
        const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
        this.httpClient.post('https://test.keepupcar.ch/backend_keepupcar/changePassword.php', Form,  { headers: headers}).subscribe(
        (result) => {
            console.log(result);
            if(result['goodPassword']){
              console.log("c'est le bon mp coté angular")
            }
            else{
              console.log('no no no not good mauvais mp')
            }
        },
        (error) => {
          console.log('error');
          console.log(error);
      })
    } 
}
