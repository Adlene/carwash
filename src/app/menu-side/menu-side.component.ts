import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Subscription } from 'rxjs';
import {Router} from "@angular/router";

@Component({
  selector: 'app-menu-side',
  templateUrl: './menu-side.component.html',
  styleUrls: ['./menu-side.component.scss']
})

export class MenuClientComponent implements OnInit {
  
  getUser(){
    return JSON.parse(localStorage.getItem('user'));
  }
   currentUser ;
  isAuth: boolean;
  
  
  constructor(private userService: UserService, private router:Router) { }
  ngOnInit() {
    this.currentUser = this.getUser();
    if(this.currentUser ){
      console.log(this.currentUser);
      return this.isAuth == true;
    }
    else{
      console.log("pas d'utilisateur enregistré")
    }
  }

  logOut() {
    console.log('Tentative de déconnexion');
    localStorage.removeItem('user');
    this.router.navigate(['']);
  }

}


