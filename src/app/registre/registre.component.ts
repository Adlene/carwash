import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AbstractControl, NgForm, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { SearchCountryField, TooltipLabel, CountryISO } from 'ngx-intl-tel-input';
import { User } from '../modeles/user';
import { invalid } from '@angular/compiler/src/render3/view/util';
import { MatDialog } from '@angular/material/dialog';
import { ValidationInfoModalComponent } from '../validation-info-modal/validation-info-modal.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LOADIPHLPAPI } from 'dns';
import { AuthenticationService } from '../services/authentication.service';


@Component({
  selector: 'app-registre',
  templateUrl: './registre.component.html',
  styleUrls: ['./registre.component.scss']
})

export class RegistreComponent implements OnInit {
  currentUser:User;
  userForm: FormGroup;
  phone:any;
  constructor(public dialogRef: MatDialogRef<RegistreComponent>, private formBuilder:FormBuilder, public dialog: MatDialog,
    private httpClient: HttpClient, private authentificationService: AuthenticationService) { }
  
  preUserType = '';
  SearchCountryField = SearchCountryField;
	TooltipLabel = TooltipLabel;
	CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
  
  ngOnInit() {
    this.initForm();
  }
  
  initForm(){
    this.userForm = this.formBuilder.group({
      type: ['Client'],
      nom: ['', Validators.required],
      prenom:['', Validators.required],
      email:['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.maxLength(20), Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.maxLength(20), Validators.minLength(6)]],
      phone: ['', Validators.required],
      entrepriseCheck:[false,[]],
      isValid:false,
      inputEntreprise:'',
      inputEntrepriseTVA:'',
      inputEntreprisePresta:'',
      inputEntrepriseAdressePresta:'',
      inputEntrepriseTVAPresta:'',
      inputEntrepriseIBANPresta:'',
    }
    , {validator: this.passwordConfirming}
  )
  }
  changePreferredCountries() {
    this.preferredCountries = [CountryISO.Switzerland, CountryISO.France];
  }
  onEntrepriseCheckChange(){
    if (this.userForm.value['entrepriseCheck']) {
      this.userForm.addControl('inputEntreprise', new FormControl('', Validators.required));
      this.userForm.addControl('inputEntrepriseTVA', new FormControl('', Validators.required));
    } else{
      this.userForm.removeControl('inputEntreprise');
      this.userForm.removeControl('inputEntrepriseTVA');
    }
  }
  
  passwordConfirming(c: AbstractControl): { invalid: boolean } {
    if (c.get('password').value !== c.get('confirmPassword').value) {
        return {invalid: true};
    }
  }
  

  submitForm(){
   
  const formValue = this.userForm.value;
  this.currentUser = new User(
    formValue['type'],
    formValue['avatar'], // TODO: AJOUTER avatar
    formValue['nom'],
    formValue['prenom'],
    formValue['email'],
    formValue['password'],
    formValue['phone']["e164Number"],
    formValue['entrepriseCheck'],
    formValue['isValid'],
    formValue['inputEntreprise'],
    formValue['inputEntrepriseTVA'],
    formValue['inputEntreprisePresta'],
    formValue['inputEntrepriseAdressePresta'],
    formValue['inputEntrepriseTVAPresta'],
    formValue['inputEntrepriseIBANPresta'],    
  );
  this.authentificationService.createNewUser(this.currentUser, () => {
    this.openDialog();
  });
  
  console.log(this.currentUser);
  this.dialogRef.close();
}
findValue(form:NgForm){
  console.log(this.preUserType); 
}
openDialog(): void {
  const dialogRef = this.dialog.open(ValidationInfoModalComponent, {
    width: '300px',
    height: '200px',
  })
}
} 
