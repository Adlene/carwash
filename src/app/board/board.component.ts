import { Component, OnInit, ViewChild } from '@angular/core';
import { CarService } from '../services/car.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbSlideEvent , NgbCarousel, NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { CarFormComponent} from '../car-form/car-form.component'
import { User } from '../modeles/user';
import { AuthenticationService } from '../services/authentication.service';
import { Subscription } from 'rxjs';



@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
  providers: [NgbCarouselConfig]
})

export class BoardComponent implements OnInit {
  //reference to "myCarousel"
  // @ViewChild('carousel', {static : true}) carousel: NgbCarousel;$
  currentUser: User;
  
  
  
  currentCarId = 0; // init 
  cars:any[] = [];
  currentUserSubscription: Subscription;
  constructor( private carService:CarService, private config: NgbCarouselConfig, public dialog: MatDialog, private authService: AuthenticationService) {
    this.config.interval = 2000;
    this.config.wrap = true;
    this.config.keyboard = true;
    this.config.pauseOnHover = true;
  } 
  openDialog(): void {
    const dialogRef = this.dialog.open(CarFormComponent, {
      width: '600px',
      height: '1000px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  ngOnInit() {
    this.cars = this.carService.cars;
      this.currentUserSubscription = this.authService.currentUserSubject.subscribe(
        currentUserFromSubject => {
          this.currentUser = currentUserFromSubject; 
        }
      );
      this.authService.emitUserAndIsAuth();
    
    //TODO: get NGBCAROUSEL ACTIVE ID FOR PROPER INIT
  }
 
  colorPb (a:number){
    if(a>50){
      return 'success';
    }
    else if (a<50 && a>25){
      return 'warning';
    }
    else if (a<=25){
      return 'danger';
    }
  }
  onSlide(slideEvent: NgbSlideEvent) {
    console.log('slide no : ' +slideEvent.current);
    //Splitting it with : as the separator
    var currentCarId = slideEvent.current.split("ngb-slide-");
    console.log('slide no : ' +currentCarId[1]);
  
    console.log(this.cars[currentCarId[1]].name);
    this.currentCarId = +currentCarId[1];
  }
  
  clickOnCarousel(carousel: NgbCarousel) {
    // get carousel by ref 
    console.log('activeId' +carousel.activeId);
    carousel.pause();
  }
}
