import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarificationFormPrestaComponent } from './tarification-form-presta.component';

describe('TarificationFormPrestaComponent', () => {
  let component: TarificationFormPrestaComponent;
  let fixture: ComponentFixture<TarificationFormPrestaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarificationFormPrestaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarificationFormPrestaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
