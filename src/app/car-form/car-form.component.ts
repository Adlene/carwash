import { Component, OnInit } from '@angular/core';
import { fakeCarBddService } from '../services/fakeCarBdd.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-car-form',
  templateUrl: './car-form.component.html',
  styleUrls: ['./car-form.component.scss']
})
export class CarFormComponent implements OnInit {

  fakeCarBdd = [];
  currentMarque;
  filteredCar = [];
  couleur = ['rouge', 'bleu', 'vert', 'jaune', 'gris monochrome de jouvance', 
  'orange turquoise', 'jaune smoothie ananas', ' couleur coloré', 'passion saumon', 'blanc panini jambon de parme']
  constructor( private fakeCarBddService: fakeCarBddService, //public activeModal: NgbActiveModal
    ) { }

  ngOnInit() {
    this.fakeCarBdd = this.fakeCarBddService.categories;
    //console.log(this.fakeCarBddService.groupBy());
  }

  onSelectCar(cat) {
    this.currentMarque = cat;
    console.log(this.currentMarque);
    const result = this.fakeCarBddService.fakeCarBdd.filter(car => car.marque == this.currentMarque);
    this.filteredCar = result;
    console.log(result);
  }

  open() {
    //const modalRef = this.modalService.open(NgbdModalContent);
    //modalRef.componentInstance.name = 'World';
 }

}
