import { Component, OnInit } from '@angular/core';
import { PrestataireService } from '../services/prestataire.service';
import { CarService } from '../services/car.service';

@Component({
  selector: 'app-presta-form',
  templateUrl: './presta-form.component.html',
  styleUrls: ['./presta-form.component.scss']
})
export class PrestaFormComponent implements OnInit {
presta = [];
car = []
  constructor( private prestataireService:PrestataireService, private carService: CarService) { }

  ngOnInit() {
    this.presta = this.prestataireService.prestataire;
    this.car = this.carService.cars;
  }

}
