import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardPrestaComponent } from './board-presta.component';

describe('BoardPrestaComponent', () => {
  let component: BoardPrestaComponent;
  let fixture: ComponentFixture<BoardPrestaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardPrestaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardPrestaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
