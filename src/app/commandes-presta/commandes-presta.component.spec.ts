import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommandesPrestaComponent } from './commandes-presta.component';

describe('CommandesPrestaComponent', () => {
  let component: CommandesPrestaComponent;
  let fixture: ComponentFixture<CommandesPrestaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommandesPrestaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommandesPrestaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
