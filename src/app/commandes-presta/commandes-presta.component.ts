import { Component, OnInit, PipeTransform  } from '@angular/core';
//import { fakeCommandesPrestaService } from '../services/fakeCommandePrestaBdd.service';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { Observable, from } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';
import { DetailCommandeComponent } from '../detail-commande/detail-commande.component';

interface fakeCommandesPresta{
    id : number;
    statut : string;
    client : string;
    lieu : string;
    commentaire: string;
    horaire: string;
    vehicule: string;
    type: string;
}

const FAKECOMMANDESPRESTAS: fakeCommandesPresta[] = [
  {
    id : 1,
    statut : 'Demandé',
    client : 'Menot',
    lieu : 'Société Google',
    commentaire: 'Récupérer les clés à la réception',
    horaire: 'matin',
    vehicule: 'Aston Martin db11',
    type: 'intérieur'        
},
{
    id : 2,
    statut : 'Payé',
    client : 'Murciano',
    lieu : 'Parking rue de Berne',
    commentaire: '', 
    horaire: 'matin',
    vehicule: 'Audi Rs4',
    type: 'intérieur'  
},
{
    id : 3,
    statut : 'Effectué',
    client : 'Musk',
    lieu : 'Antenne Tesla Geneve',
    commentaire : 'Mot de passe ouverture: armagedon',  
    horaire: 'matin',
    vehicule: 'Lamborghini Aventador',
    type: 'extérieur'  
},
{
    id : 4,
    statut : 'Effectué',
    client : 'Musk',
    lieu : 'Antenne Tesla Geneve',
    commentaire : 'Mot de passe ouverture: armagedon',
    horaire: 'apres-midi',
    vehicule: 'Aston Martin db11',
    type: 'intérieur'  
},
{
    id : 5,
    statut : 'Effectué',
    client : 'Dupond',
    lieu : 'ONU Geneve',
    commentaire : 'Clés au poste de gardiennage entrée nord ',
    horaire: 'matin',
    vehicule: 'Bugatti Chiron',
    type: 'intérieur'  
},
{
    id : 6,
    statut : 'Demandé',
    client : 'Winston',
    lieu : 'Palais Winston',
    commentaire : '',
    horaire: 'matin',
    vehicule: 'Nissan Quashquai',
    type: 'extérieur'  
},
{
    id : 7,
    statut : 'Effectué',
    client : 'Spring',
    lieu : 'Conforama Meyrin',
    commentaire : '',
    horaire: 'matin',
    vehicule: 'Bentley continental GT',
    type: 'intérieur'  
}
]

function search(text: string, pipe: PipeTransform): fakeCommandesPresta[] {
  return FAKECOMMANDESPRESTAS.filter(fakeCommandesPresta => {
    const term = text.toLowerCase();
    return fakeCommandesPresta.statut.toLowerCase().includes(term)
        || pipe.transform(fakeCommandesPresta.vehicule).includes(term)
        || pipe.transform(fakeCommandesPresta.type).includes(term);
  });
}

@Component({
  selector: 'app-commandes-presta',
  templateUrl: './commandes-presta.component.html',
  styleUrls: ['./commandes-presta.component.scss'],
  providers: [DecimalPipe]
})
export class CommandesPrestaComponent implements OnInit {

  fakeCommandesPresta$ : Observable<fakeCommandesPresta[]>;
  filter = new FormControl('');

  constructor( pipe: DecimalPipe, public dialog: MatDialog) {
    this.fakeCommandesPresta$ = this.filter.valueChanges.pipe(
      startWith(''),
      map(text => search(text, pipe))
    );
   }
   openDialog(): void {
    const dialogRef = this.dialog.open(DetailCommandeComponent, {
      width: '65%',
      height: '95%',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  ngOnInit() {
    
    function getColor(){
      if(this.fakeCommandesPresta.values === 'Demandé'){
        return 'red';
      }
    }
  }

}
